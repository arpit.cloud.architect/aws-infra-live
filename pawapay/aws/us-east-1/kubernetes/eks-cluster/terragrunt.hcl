terraform {
  source = "git::git@gitlab.com:arpit.cloud.architect/aws-infra-modules//eks-service-aws"
}

include {
  path = find_in_parent_folders()
}

locals {
  services_vars = read_terragrunt_config(find_in_parent_folders("services.hcl"))
}

dependency network {
  config_path = "../../networking"
}

inputs = {
  public_subnet_ids  = [dependency.network.outputs.public_subnet_ids["public-k8s-subnet-b"].id, dependency.network.outputs.public_subnet_ids["public-k8s-subnet-c"].id]
  private_subnet_ids = [dependency.network.outputs.private_subnet_ids["private-k8s-subnet-b"].id, dependency.network.outputs.private_subnet_ids["private-k8s-subnet-c"].id]
  eks_cluster_name   = local.services_vars.locals.stack_name
  public_access      = true
  private_access     = false
  allowed_cidr       = ["0.0.0.0/0"]
  autoscaling-config = ({
    desired_size   = 1
    max_size       = 2
    min_size       = 1
    ami_type       = "AL2_x86_64"
    capacity_type  = "ON_DEMAND"
    disk_size      = 20
    instance_types = ["c4.xlarge"]
    version        = "1.21"
  })
  readonlyusername = ["IAM-IaC-SDLC-EKS_readonly_user"]
  adminusername    = ["IAM-IaC-SDLC-EKS_admin_user"]
}