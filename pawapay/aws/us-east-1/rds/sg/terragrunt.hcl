terraform {
  source = "git@gitlab.com:arpit.cloud.architect/aws-infra-modules.git//aws-sg"
}

include {
  path = find_in_parent_folders()
}

dependency network {
  config_path = "../../networking"
}

inputs = {
  name        = "pawapay-rds-ec2-sg"
  description = "Security group rds"
  vpc_id      = dependency.network.outputs.vpc_id.id
  ingress_cidr_blocks      = ["0.0.0.0/0"]
  ingress_rules            = ["mysql-tcp"]
  egress_rules = ["all-all"]
}
