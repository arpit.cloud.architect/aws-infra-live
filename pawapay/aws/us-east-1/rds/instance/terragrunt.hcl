terraform {
  source = "git::git@gitlab.com:arpit.cloud.architect/aws-infra-modules//aws-rds"
}

include {
  path = find_in_parent_folders()
}
dependency network {
  config_path = "../../networking"
}
locals {
  provider_vars  = read_terragrunt_config(find_in_parent_folders("provider.hcl"))
}

inputs = {
  identifier = "pawapay"
  engine            = "mysql"
  engine_name       = "mysql"
  engine_version    = "5.7.19"
  instance_class    = "db.t2.large"
  allocated_storage = 5
  publicly_accessible = true

  name     = "demodb"
  username = "user"
  password = "YourPwdShouldBeLongAndSecure!"
  port     = "3306"

  iam_database_authentication_enabled = true

  vpc_security_group_ids = ["sg-010da9a8460d04d63"]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"
  monitoring_interval = "30"
  monitoring_role_name = "MyRDSMonitoringRole"
  create_monitoring_role = true

  tags = {
    Owner       = "user"
    Environment = "dev"
  }
  subnet_ids = [for value in dependency.network.outputs.public_subnet_ids : value.id]
  family = "mysql5.7"
  major_engine_version = "5.7"
  deletion_protection = true

  parameters = [
    {
      name = "character_set_client"
      value = "utf8mb4"
    },
    {
      name = "character_set_server"
      value = "utf8mb4"
    }
  ]

  options = [
    {
      option_name = "MARIADB_AUDIT_PLUGIN"

      option_settings = [
        {
          name  = "SERVER_AUDIT_EVENTS"
          value = "CONNECT"
        },
        {
          name  = "SERVER_AUDIT_FILE_ROTATIONS"
          value = "37"
        },
      ]
    },
  ]
}