terraform {
  source = "git::git@gitlab.com:arpit.cloud.architect/aws-infra-modules//ecr-repositories-aws"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  ecr_permissions = file("files/policy.json")
  repositories = {
    notebook = {
      stack       = "pawaypay-image"
      app         = "payment"
    }
  }
}