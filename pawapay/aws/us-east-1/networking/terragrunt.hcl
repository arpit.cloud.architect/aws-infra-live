terraform {
  source = "git::git@gitlab.com:arpit.cloud.architect/aws-infra-modules//networking-aws"
}

include {
  path = find_in_parent_folders()
}

locals {
  provider_vars  = read_terragrunt_config(find_in_parent_folders("provider.hcl"))
}
#prevent_destroy = true

inputs = {
  vpc = {
    name                    = local.provider_vars.locals.aws_main_vpc_name
    cidr_block              = "10.22.0.0/16"
    nat_gateway_subnet_name = "public-dev-a"
    private_subnets = {
      private-dev-a = {
        cidr_block        = "10.22.0.0/24"
        availability_zone = "us-east-1a"
      }
      private-dev-b = {
        cidr_block        = "10.22.1.0/24"
        availability_zone = "us-east-1b"
      }
      private-k8s-subnet-b = {
        cidr_block        = "10.22.6.0/24"
        availability_zone = "us-east-1b"
      }
      private-k8s-subnet-c = {
        cidr_block        = "10.22.7.0/24"
        availability_zone = "us-east-1c"
      }
    }
    public_subnets = {
      public-dev-a = {
        cidr_block        = "10.22.3.0/24"
        availability_zone = "us-east-1a"
      }
      public-dev-b = {
        cidr_block        = "10.22.4.0/24"
        availability_zone = "us-east-1b"
      }
      public-k8s-subnet-b = {
        cidr_block        = "10.22.8.0/24"
        availability_zone = "us-east-1b"
      }
      public-k8s-subnet-c = {
        cidr_block        = "10.22.9.0/24"
        availability_zone = "us-east-1c"
      }
    }
  }
}