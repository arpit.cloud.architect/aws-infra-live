# Configure Terragrunt to automatically store tfstate files in an S3 bucket
locals {
  provider_vars    = read_terragrunt_config(find_in_parent_folders("provider.hcl"))
  datacenter_vars  = read_terragrunt_config(find_in_parent_folders("datacenter.hcl"))
  environment_vars = read_terragrunt_config(find_in_parent_folders("environment.hcl"))
}

remote_state {
  backend = "s3"
  config = {
    bucket         = "terragrunt-dev"
    key            = "${path_relative_to_include()}//terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terragrunt-dev-locks"
    encrypt        = true
    profile        = "arpit_cred"
  }
}

inputs = merge(
  local.provider_vars.locals,
  local.datacenter_vars.locals,
  local.environment_vars.locals,
)